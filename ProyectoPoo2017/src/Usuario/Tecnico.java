/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Usuario;

import Empresa.Cliente;
import Empresa.Proveedor;
import Miscelaneos.Archivo;
import Miscelaneos.Ip;
import Miscelaneos.Pais;
import Miscelaneos.Tarifa;
import Miscelaneos.Llamada;
import Miscelaneos.LlamadaFacturada;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import proyectopoo2017.ProyectoPoo2017;
import static proyectopoo2017.ProyectoPoo2017.listaClientes;
import static proyectopoo2017.ProyectoPoo2017.listaIps;
import static proyectopoo2017.ProyectoPoo2017.listaPaises;
import static proyectopoo2017.ProyectoPoo2017.listaProveedores;
import static proyectopoo2017.ProyectoPoo2017.listaTarifas;

/**
 *
 * @author David
 */
public class Tecnico extends Usuario {
   Archivo archivo=new Archivo();

    public Tecnico() {
    }

    public Tecnico(String usuario, String contraseña, String nombre, String nivel) {
        super(usuario, contraseña, nombre, nivel);
    }
    
    
    public void mostrarClientes(ArrayList<Cliente> lista){
        for(Cliente i:lista){
            System.out.println(i.toString());
        }
    }
    public ArrayList<Cliente> editarClientes(ArrayList<Cliente> lista){
        
        System.out.println("Ingrese el codigo del cliente que desea editar");
        Scanner input= new Scanner(System.in);
        String cod=input.next();
        boolean bandera=false;
        for(Cliente e:lista){
            if(e.getCodigo().equals(cod)){
                System.out.println("Tipo de cliente:");
                String tcliente=input.next();
                e.setTipoCliente(tcliente);
                System.out.println("Nombre del cliente:");
                String ncliente=input.next();
                e.setNombre(ncliente);
                System.out.println("Dirección:");
                String dcliente=input.next();
                e.setDireccion(dcliente);
                System.out.println("Telefono:");
                String tecliente=input.next();
                e.setTelefono(tecliente);
                bandera=true;
                archivo.editarArchivo(listaClientes,"clientes");
            }
        }
        if(!bandera){
            System.out.println("No hay un cliente registrado con ese codigo. Desea agregar un nuevo cliente?(si o no)");
            String respuesta=input.next();
            
            while (!respuesta.equals("si")|!respuesta.equals("no")) {
                if(respuesta.equals("si")){
                    System.out.println("Tipo del cliente:");
                    String tcliente=input.next();
                    System.out.println("Nombre del cliente:");
                    String ncliente=input.next();
                    System.out.println("Dirección:");
                    String dcliente=input.next();
                    System.out.println("Telefono:");
                    String tecliente=input.next();
                    lista.add(new Cliente(tcliente,cod,ncliente,dcliente,tecliente));
                }
                else if (respuesta.equals("no")) {
                    break;
                } else {System.out.println("Ingrese si o no: "); respuesta = input.next();}
            } 
        }  
        return lista;
    }
    
    public void mostrarProveedores(ArrayList<Proveedor> lista){
        for(Proveedor i:lista){
            System.out.println(i.toString());
        }
    }
    public ArrayList<Proveedor> editarProveedores(ArrayList<Proveedor> lista){
        System.out.println("Ingrese el codigo del proveedor que desea editar");
        Scanner input= new Scanner(System.in);
        String cod=input.next();
        boolean bandera=false;
        for(Proveedor e:lista){
            if(e.getCodigo().equals(cod)){
                System.out.println("Nombre del proveedor:");
                String nproveedor=input.next();
                e.setNombre(nproveedor);
                System.out.println("Dirección:");
                String dproveedor=input.next();
                e.setDireccion(dproveedor);
                System.out.println("Telefono:");
                String teproveedor=input.next();
                e.setTelefono(teproveedor);
                bandera=true;
                archivo.editarArchivo(listaProveedores,"proveedores");
            }
        }
        if(!bandera){
            System.out.println("No hay un proveedor registrado con ese codigo. Desea agregar un nuevo proveedor?(si o no)");
            String respuesta=input.next();
            if(respuesta.equals("si")){
                System.out.println("Nombre del proveedor:");
                String nproveedor=input.next();
                System.out.println("Dirección:");
                String dproveedor=input.next();
                System.out.println("Telefono:");
                String tproveedor=input.next();
                lista.add(new Proveedor(cod,nproveedor,dproveedor,tproveedor));

            }
        }
        
        return lista;
    }
    
    public void mostrarPaises(ArrayList<Pais> lista){
        for(Pais i:lista){
            System.out.println(i.toString());
        }
    }
    
    public ArrayList<Pais> editarPaises(ArrayList<Pais> lista){
        System.out.println("Ingrese el codigo del país que desea editar");
        Scanner input= new Scanner(System.in);
        String cod=input.next();
        boolean bandera=false;
        for(Pais e:lista){
            if(e.getCodigo().equals(cod)){
                System.out.println("Nombre del país:");
                String npais=input.next();
                e.setNombre(npais);
                System.out.println("Prefijo del país:");
                String prepais=input.next();
                e.setPrefijo(prepais);
                bandera=true;
                 archivo.editarArchivo(listaPaises,"paises");
            }
        }
        if(!bandera){
            System.out.println("No hay un país registrado con ese codigo. Desea agregar un nuevo país?(si o no)");
            String respuesta=input.next();
            if(respuesta.equals("si")){
                System.out.println("Nombre del país:");
                String npais=input.next();
                System.out.println("Prefijo del país:");
                String prepais=input.next();
                lista.add(new Pais(cod,npais,prepais));
            }
        }
            
        return lista;
    }
    
    public void mostrarIps(ArrayList<Ip> lista){
        for(Ip i:lista){
            System.out.println(i.toString());
        }
    }
    
    public ArrayList<Ip> editarIps(ArrayList<Ip> listaIp){
        System.out.println("Ingrese la ip que desea editar");
        Scanner input= new Scanner(System.in);
        String ip=input.next();
        boolean bandera=false;
        for(Ip e:listaIp){
            if(e.getIp().equals(ip)){
                System.out.println("Tipo de asignacion(P si es proveedor o C si es cliente:");
                String tasig=input.next();
                e.setTipo(tasig);
                System.out.println("Código de Cliente/Proveedor:");
                String cod=input.next();
                e.setCodigo(cod);
                bandera=true;
                archivo.editarArchivo(listaIps,"ips");
            }
        }
        if(!bandera){
            System.out.println("No se encuentra registrada esa Ip.");
            
        }
        return listaIp;
    }
    
    public ArrayList<Ip> agregarIps(ArrayList<Ip> listaIp){
        boolean bandera=true;
        while(bandera){
        
        Scanner input= new Scanner(System.in);
        System.out.println("Ip:");
        String ip=input.nextLine();
        String[] listIp = ip.split("\\.");
        Boolean flag = true;
        while(flag) { 
        short cont = 0;
            
            for (String i: listIp) {
                if (i.length()<=3) {
                    cont +=1;
                }
            }
            if (cont!=4){
                System.out.println("Ingrese un IP válido: ");
                ip=input.next();
                listIp = ip.split(".");
            } else flag = false;
        }              
        boolean bandera2=true;
        for (Ip e:listaIp){
           if (e.getIp().equals(ip)){
               bandera2=false;
               
           } 
        }
        if (bandera2){
            System.out.println("Ingrese tipo de asignación. (C-Cliente o P-Proveedor):");
            String tagente=input.next();
            while (!(tagente.equals("C")||tagente.equals("P"))){
                System.out.println("ERROR. (C-Cliente o P-Proveedor):");
                tagente=input.next();
            }
            System.out.println("Ingrese el código del cliente o proveedor:");
            String cagente= input.next();
            listaIp.add(new Ip(ip,tagente,cagente));
            bandera=false;
        } else System.out.println("Ya existe esa Ip. Por favor ingrese uno nuevo.");
        }
        return listaIp;
         
    }
    
    public void mostrarTarifas(ArrayList<Tarifa> lista){
        for(Tarifa i:lista){
            System.out.println(i.toString());
        }
    }
    
    public ArrayList<Tarifa> editarTarifas(ArrayList<Tarifa> lista){
        System.out.println("Ingrese el código de la tarifa que desea editar");
        mostrarTarifas(lista);
        Scanner input= new Scanner(System.in);
        String cod=input.next();
        boolean bandera=false;
        for(Tarifa e:lista){
            if(e.getCodigoTarifa().equals(cod)){
                System.out.println("Nuevo valor de la tarifa:");
                Float ntarifa=input.nextFloat();
                e.setTarifa(ntarifa);
                bandera=true;
                archivo.editarArchivo(listaTarifas,"tarifas");
            }
        }
        if(!bandera){
            System.out.println("No exite el código de tarifa ingresado");
        }    
        return lista;
    }
    
    public ArrayList<LlamadaFacturada> facturarLlamadas(ArrayList<Llamada> listaLlamadas, ArrayList<Ip> listaIp, ArrayList<Tarifa> listaTarifas, ArrayList<Cliente> listaClientes) throws IOException {
        ArrayList<LlamadaFacturada> llamadasFacturadas = new ArrayList<>();
        float tarifa = 0;
        float costo_proveedor = 0;
        float costo_cliente = 0;
        String codigo_pais="";
        String codigo_proveedor="";
        String codigo_cliente="";
        String estado="";
        for(Llamada l: listaLlamadas) {
            for (Ip ip: listaIp) {
                if (l.getIpFuente().equals(ip.getIp())) {
                    
                    codigo_cliente = ip.getCodigo();
                }
                if (l.getIpDestino().equals(ip.getIp())) {
                    codigo_proveedor = ip.getCodigo();   
                }
            }
            if (l.getEstado().equals("E")) {
                estado = "n";
                for (Tarifa t: listaTarifas) {
                    if (l.getDnis().startsWith(t.getCodigoPais())) {
                        codigo_pais = t.getCodigoPais();
                    }
                }
                llamadasFacturadas.add(new LlamadaFacturada(l.getFecha(),l.getHora(),l.getIpFuente(),l.getIpDestino(),l.getDnis(),l.getAni(),estado,l.getDuracion(),codigo_cliente,codigo_proveedor,codigo_pais,0,0));    
            } else {
                boolean band=false;
                estado = "s";
                for (Tarifa t: listaTarifas) {
                   

                    if (l.getDnis().startsWith(t.getPrefijoRegion()) ) {
                        tarifa = t.getTarifa();
                        codigo_pais=t.getCodigoPais();
                        costo_proveedor = (l.getDuracion()/60)*tarifa;
                        band=true;
                    }   
                }
                if(!band){
                     codigo_pais = "NA";
                        tarifa=0;
                        costo_proveedor=0;
                        costo_cliente=0;
                }
                        for (Cliente c: listaClientes) {
                            if (codigo_cliente.equals(c.getCodigo())) {
                                if (c.getTipoCliente().equals("Wholesale")) {
                                    costo_cliente = (float) (costo_proveedor*1.05);
                                } else costo_cliente = (float) (costo_proveedor*1.10);
                            }
                        } 
                    
                llamadasFacturadas.add(new LlamadaFacturada(l.getFecha(),l.getHora(),l.getIpFuente(),l.getIpDestino(),l.getDnis(),l.getAni(),estado,l.getDuracion(),codigo_cliente,codigo_proveedor,codigo_pais,costo_cliente,costo_proveedor));      
            }                
        }
        archivo.crearArchivoLlamadasFacturadas(llamadasFacturadas);
        return llamadasFacturadas;
    } 
}  

