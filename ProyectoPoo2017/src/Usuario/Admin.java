/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Usuario;

import Empresa.Cliente;
import Empresa.Proveedor;
import Empresa.SumarioAgente;
import Miscelaneos.LlamadaFacturada;
import Miscelaneos.Pais;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author David
 */
public class Admin extends Usuario {

    /**
     *
     */
    public Admin() {
    }

    /**
     *
     * @param usuario
     * @param contraseña
     * @param nombre
     * @param nivel
     */
    public Admin(String usuario, String contraseña, String nombre, String nivel) {
        super(usuario, contraseña, nombre, nivel);
    }
    
    /**
     *
     * @param lf
     * @param cod
     * @return
     */
    public ArrayList<SumarioAgente> sumarioCliente(ArrayList<LlamadaFacturada> lf,String cod) {
        ArrayList<SumarioAgente> outer = new ArrayList<SumarioAgente>();
        String mes1="09";
        String mes2="10";
           SumarioAgente sum=sumarioMes( lf,cod,mes1);
           SumarioAgente sum2=sumarioMes( lf,cod,mes2);
            outer.add(sum);
            outer.add(sum2);
        return outer;
    }

    /**
     *
     * @param codigo
     * @param mes
     * @param sumarioClientes
     * @return
     */
    public ArrayList<String> detalleFactura(String codigo,String mes, ArrayList<ArrayList<SumarioAgente>> sumarioClientes, ArrayList<Cliente> listaClientes){
        Boolean band= false;
        String nombre="";
        String tipo="";
        int totalCS=0;
        double totalf=0;
        double totalP=0;
        for(Cliente c: listaClientes){
           if(codigo.equals(c.getCodigo())){
               nombre=c.getNombre();
               tipo=c.getTipoCliente();
               band=true;
               break;
       }
           else band=false;
        }
        if (band){
            for(ArrayList<SumarioAgente> a: sumarioClientes){
                for(SumarioAgente s: a){
                    totalf+=s.getCostoTotal();
                }
            }
            if(tipo.equals("Wholesale")){
                totalCS=20;
                totalP=(double)totalf+totalCS;
            }else{ 
                totalP=(double)totalf;
                totalCS=0;
            }
        }
        else System.out.println("No existe cliente");
           
       
        ArrayList<String> detalleFactura= new ArrayList<String>();
        detalleFactura.add("Cliente: "+ nombre);
        detalleFactura.add("Total llamadas facturadas : $"+ totalf);
        detalleFactura.add("Total costo de servicio : $"+ totalCS);
        detalleFactura.add("Total a pagar en el mes: $"+ totalP);
        return detalleFactura;
        
    }
    public ArrayList<SumarioAgente> sumarioProveedor(ArrayList<LlamadaFacturada> lf,String cod) {
        ArrayList<SumarioAgente> outer = new ArrayList<SumarioAgente>();
        String mes1="09";
        String mes2="10";
           SumarioAgente sum=sumarioMesProveedor( lf,cod,mes1);
           SumarioAgente sum2=sumarioMesProveedor( lf,cod,mes2);
            outer.add(sum);
            outer.add(sum2);
        return outer;
    }
    public SumarioAgente sumarioMesProveedor(ArrayList<LlamadaFacturada> lf,String cod,String mes1){
            int llamadasC = 0;
            int llamadasI = 0;
            double costoTotal = 0;
            String mes = "";
            String año = "";
            String codPais = "";
            for (LlamadaFacturada l:lf) {
                String mesl= l.getFecha().substring(5, 7);                
                if (cod.equals(l.getCodigo_proovedor())& mesl.equals(mes1)) {
                    año = l.getFecha().substring(0, 4);
                    mes=mesl;
                    codPais = l.getCodigo_pais();                    
                    if (l.getEstado().equals("s")) {
                        llamadasC+=1;
                    } else llamadasI +=1;  
                    costoTotal+=l.getCosto_proovedor();
            } 
            }
            SumarioAgente sum= new SumarioAgente(año,mes,cod,codPais,llamadasC,llamadasI,costoTotal);
            return sum;
    }

    /**
     *
     * @param lf
     * @param cod
     * @param mes1
     * @return
     */
    public SumarioAgente sumarioMes(ArrayList<LlamadaFacturada> lf,String cod,String mes1){
            int llamadasC = 0;
            int llamadasI = 0;
            double costoTotal = 0;
            String mes = "";
            String año = "";
            String codCliente = "";
            String codPais = "";
            int cont=0;
            for (LlamadaFacturada l:lf) {
                String mesl= l.getFecha().substring(5, 7);                
                if (cod.equals(l.getCodigo_cliente())& mesl.equals(mes1)) {
                    año = l.getFecha().substring(0, 4);
                    mes=mesl;
                    codPais = l.getCodigo_pais();                    
                    if (l.getEstado().equals("s")) {
                        llamadasC+=1;
                    } else llamadasI +=1;  
                    costoTotal+=l.getCosto_cliente();
            } 
            }
            SumarioAgente sum= new SumarioAgente(año,mes,cod,codPais,llamadasC,llamadasI,costoTotal);
            return sum;
    }
    public ArrayList<ArrayList<String>> reporteLlamadasClientes(String mes,ArrayList<ArrayList<SumarioAgente>> sumarioClientes, ArrayList<Cliente> listaClientes, ArrayList<Pais> Paises  ){

       ArrayList<ArrayList<String>> reporteLlamadas= new ArrayList<>();

        Boolean band= false;
        String nombre="";
        String tipo="";
        for(Cliente c: listaClientes){
             ArrayList<String> list= new ArrayList<>();
           for(ArrayList<SumarioAgente> a: sumarioClientes){
            for(SumarioAgente b: a){
               if(c.getCodigo().equals(b.getCodigo())& b.getMes().equals(mes)){
                  list.add(c.getNombre());
                  for(Pais p:Paises){
                      if(b.getCodigoPais().equals(p.getPrefijo())){
                          list.add(p.getNombre());
                      }
                }
               list.add(String.valueOf(b.getLlamadasC()));
               list.add(String.valueOf(b.getLlamadasI()));
               list.add(String.valueOf(b.getCostoTotal()));
               reporteLlamadas.add(list);
                  break;
               }
               
            }
        }  
        }
        return reporteLlamadas;
       
    }
    public ArrayList<ArrayList<String>> reporteLlamadasProveedores(String mes,ArrayList<ArrayList<SumarioAgente>> sumarioProveedores, ArrayList<Proveedor> listaProveedor, ArrayList<Pais> Paises  ){

       ArrayList<ArrayList<String>> reporteLlamadas= new ArrayList<>();

        Boolean band= false;
        String nombre="";
        String tipo="";
        for(Proveedor c: listaProveedor){
             ArrayList<String> list= new ArrayList<>();
           for(ArrayList<SumarioAgente> a: sumarioProveedores){
            for(SumarioAgente b: a){
               if(c.getCodigo().equals(b.getCodigo())& b.getMes().equals(mes)){
                  list.add(c.getNombre());
                  for(Pais p:Paises){
                      if(b.getCodigoPais().equals(p.getPrefijo())){
                          list.add(p.getNombre());
                      }
                }
               list.add(String.valueOf(b.getLlamadasC()));
               list.add(String.valueOf(b.getLlamadasI()));
               list.add(String.valueOf(b.getCostoTotal()));
               reporteLlamadas.add(list);
                  break;
               }
               
            }
        }  
        }
        return reporteLlamadas;
       
    }
              
}
