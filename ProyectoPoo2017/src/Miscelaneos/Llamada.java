/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Miscelaneos;

/**
 *
 * @author galit
 */
public class Llamada {
    private String fecha;
    private String hora;
    private String ipFuente;
    private String ipDestino;
    private String dnis;
    private String ani;
    private String estado;
    private int duracion;
    
    public Llamada() {
    }

    public Llamada(String fecha, String hora, String ipFuente, String ipDestino, String dnis, String ani, String estado, int duracion) {
        this.fecha = fecha;
        this.hora = hora;
        this.ipFuente = ipFuente;
        this.ipDestino = ipDestino;
        this.dnis = dnis;
        this.ani = ani;
        this.estado = estado;
        this.duracion = duracion;
    }
    
    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getIpFuente() {
        return ipFuente;
    }

    public void setIpFuente(String ipFuente) {
        this.ipFuente = ipFuente;
    }

    public String getIpDestino() {
        return ipDestino;
    }

    public void setIpDestino(String ipDestino) {
        this.ipDestino = ipDestino;
    }

    public String getDnis() {
        return dnis;
    }

    public void setDnis(String dnis) {
        this.dnis = dnis;
    }

    public String getAni() {
        return ani;
    }

    public void setAni(String ani) {
        this.ani = ani;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    @Override
    public String toString() {
        return fecha + "," + hora + "," + ipFuente + "," + ipDestino + "," + dnis + "," + ani + "," + estado + "," + duracion;
    }
    
    
}
