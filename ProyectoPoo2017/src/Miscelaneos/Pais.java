/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Miscelaneos;

/**
 *
 * @author David
 */
public class Pais {
    public String codigo;
    public String nombre;
    public String prefijo;

    public Pais() {
    }

    public Pais(String codigo, String nombre, String prefijo) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.prefijo = prefijo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    @Override
    public String toString() {
        return "Pais{" + "codigo=" + codigo + ", nombre=" + nombre + ", prefijo=" + prefijo + '}';
    }
    
    
    
}
