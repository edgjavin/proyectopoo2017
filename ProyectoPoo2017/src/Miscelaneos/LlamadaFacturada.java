/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Miscelaneos;

/**
 *
 * @author galit
 */
public class LlamadaFacturada extends Llamada{
    private String codigo_cliente;
    private String codigo_proovedor;
    private String codigo_pais;
    private float costo_cliente;
    private float costo_proovedor;

    public LlamadaFacturada(String fecha, String hora, String ipFuente, String ipDestino, String dnis, String ani, String estado, int duracion,String codigo_cliente, String codigo_proovedor, String codigo_pais, float costo_cliente, float costo_proovedor) {
        super(fecha, hora, ipFuente, ipDestino, dnis, ani, estado, duracion);
        this.codigo_cliente = codigo_cliente;
        this.codigo_proovedor = codigo_proovedor;
        this.codigo_pais = codigo_pais;
        this.costo_cliente = costo_cliente;
        this.costo_proovedor = costo_proovedor;
    }

    public String getCodigo_cliente() {
        return codigo_cliente;
    }

    public void setCodigo_cliente(String codigo_cliente) {
        this.codigo_cliente = codigo_cliente;
    }

    public String getCodigo_proovedor() {
        return codigo_proovedor;
    }

    public void setCodigo_proovedor(String codigo_proovedor) {
        this.codigo_proovedor = codigo_proovedor;
    }

    public String getCodigo_pais() {
        return codigo_pais;
    }

    public void setCodigo_pais(String codigo_pais) {
        this.codigo_pais = codigo_pais;
    }

    public double getCosto_cliente() {
        return costo_cliente;
    }

    public void setCosto_cliente(float costo_cliente) {
        this.costo_cliente = costo_cliente;
    }

    public double getCosto_proovedor() {
        return costo_proovedor;
    }

    public void setCosto_proovedor(float costo_proovedor) {
        this.costo_proovedor = costo_proovedor;
    }

    @Override
    public String toString() {
        return super.toString()+","+ codigo_cliente + "," + codigo_proovedor + "," + codigo_pais + "," + String.format("%.4f", costo_cliente) + "," + String.format("%.4f", costo_proovedor) ;

    }
    
    
   
    
    
    
    
}
