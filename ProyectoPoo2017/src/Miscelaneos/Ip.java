/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Miscelaneos;

/**
 *
 * @author David
 */
public class Ip {
    private String ip;
    private String tipo;
    private String codigo;

    public Ip() {
    }

    public Ip(String ip, String tipo, String codigo) {
        this.ip = ip;
        this.tipo = tipo;
        this.codigo = codigo;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Override
    public String toString() {
        return   ip + "," + tipo + "," + codigo;
    }
    
    
    
}
