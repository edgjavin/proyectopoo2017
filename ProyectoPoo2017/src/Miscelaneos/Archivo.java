/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Miscelaneos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Javier
 */
public class Archivo {

    public Archivo() {
    }
    public List leerArchivo(String ruta){
      File archivo = null;
      FileReader fr = null;
      BufferedReader br = null;
      List<String> list=new ArrayList<String>();
      String s="src/Archivos/"+ruta+".csv";
      archivo = new File (s);
      if (archivo.exists()){
    try {
         fr = new FileReader (archivo);
         br = new BufferedReader(fr);
         String linea;
         while((linea=br.readLine())!=null)
            list.add(linea);
      }
      catch(Exception e){
         e.printStackTrace();
      }
      return list;
    }
    return list;

}
    public void editarArchivo(ArrayList list, String ruta){
         FileWriter fichero = null;
        PrintWriter pw = null;
         try
        {
            fichero = new FileWriter("src/Archivos/"+ruta+".csv",false);
            pw = new PrintWriter(fichero);
            pw.println(list.get(0).toString());


        } catch (Exception e) {
            e.printStackTrace();
        }finally {
           try {
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        } 
           int cont=1;
        for(Object c: list){
            if(cont>=2){
                  try
        {
            fichero = new FileWriter("src/Archivos/"+ruta+".csv",true);
            pw = new PrintWriter(fichero);
                pw.println(c.toString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }
          }
            cont++;
        }
        }
    public void crearArchivoLlamadasFacturadas(ArrayList<LlamadaFacturada> list) throws IOException{
        File file=new File("src/Archivos/llamadas_facturadas.csv");
        if (!file.exists()){
            file.createNewFile();
        }
        FileWriter fichero = null;
        PrintWriter pw = null;
         try
        {
            fichero = new FileWriter("src/Archivos/llamadas_facturadas.csv",false);
            pw = new PrintWriter(fichero);
            pw.println(list.get(0).toString());

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
           try {
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        } 
           int cont=1;
        for(Object c: list){
            if(cont>=2){
                  try
        {
            fichero = new FileWriter("src/Archivos/llamadas_facturadas.csv",true);
            pw = new PrintWriter(fichero);
                pw.println(c.toString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }
          }
            cont++;
        }
    }
    }
