/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Empresa;

/**
 *
 * @author Javier
 */
public class Proveedor extends Agente {

    public Proveedor(String codigo, String nombre, String direccion, String telefono) {
        super(codigo, nombre, direccion, telefono);
    }

    public Proveedor() {
    }
@Override
    public String toString() {
        return getCodigo()+","+getNombre()+","+getDireccion()+","+getTelefono();
    }
  
    
}
