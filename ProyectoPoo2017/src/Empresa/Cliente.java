/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Empresa;

/**
 *
 * @author Javier
 */
public class Cliente extends Agente{
    private String tipoCliente;

    public String getTipoCliente() {
        return tipoCliente;
    }

    public void setTipoCliente(String tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

    public Cliente( String codigo, String nombre, String direccion, String telefono, String tipoCliente) {
        super(codigo, nombre, direccion, telefono);
        this.tipoCliente = tipoCliente;
    }

    public Cliente(String tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

    public Cliente() {
    }

    @Override
    public String toString() {
        return getCodigo()+","+getNombre()+","+getDireccion()+","+getTelefono()+","+getTipoCliente();
    }
    
    
}
