/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Empresa;

/**
 *
 * @author Javier
 */
public class Tarifa {
    private String codigoProveedor;
    private String codigoPais;
    private String nombreRegion;
    private String prefijoRegion;
    private float tarifa;

    public Tarifa(String codigoProveedor, String codigoPais, String nombreRegion, String prefijoRegion, float tarifa) {
        this.codigoProveedor = codigoProveedor;
        this.codigoPais = codigoPais;
        this.nombreRegion = nombreRegion;
        this.prefijoRegion = prefijoRegion;
        this.tarifa = tarifa;
    }

    public Tarifa() {
    }

    public String getCodigoProveedor() {
        return codigoProveedor;
    }

    public void setCodigoProveedor(String codigoProveedor) {
        this.codigoProveedor = codigoProveedor;
    }

    public String getCodigoPais() {
        return codigoPais;
    }

    public void setCodigoPais(String codigoPais) {
        this.codigoPais = codigoPais;
    }

    public String getNombreRegion() {
        return nombreRegion;
    }

    public void setNombreRegion(String nombreRegion) {
        this.nombreRegion = nombreRegion;
    }

    public String getPrefijoRegion() {
        return prefijoRegion;
    }

    public void setPrefijoRegion(String prefijoRegion) {
        this.prefijoRegion = prefijoRegion;
    }

    public float getTarifa() {
        return tarifa;
    }

    public void setTarifa(float tarifa) {
        this.tarifa = tarifa;
    }

    @Override
    public String toString() {
        return codigoProveedor + "," + codigoPais + "," + nombreRegion + "," + prefijoRegion + "," + tarifa;
    }
    
}
