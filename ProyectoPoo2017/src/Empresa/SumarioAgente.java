/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Empresa;

/**
 *
 * @author Javier
 */
public class SumarioAgente {
    private String año;
    private String mes;
    private String codigo;
    private String codigoPais;
    private int llamadasC;
    private int llamadasI;
    private double costoTotal;

    public SumarioAgente(String año, String mes, String codigo, String codigoPais, int llamadasC, int llamadasI, double costoTotal) {
        this.año = año;
        this.mes = mes;
        this.codigo = codigo;
        this.codigoPais = codigoPais;
        this.llamadasC = llamadasC;
        this.llamadasI = llamadasI;
        this.costoTotal = costoTotal;
    }

    public String getAño() {
        return año;
    }

    public void setAño(String año) {
        this.año = año;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigoPais() {
        return codigoPais;
    }

    public void setCodigoPais(String codigoPais) {
        this.codigoPais = codigoPais;
    }

    public int getLlamadasC() {
        return llamadasC;
    }

    public void setLlamadasC(int llamadasC) {
        this.llamadasC = llamadasC;
    }

    public int getLlamadasI() {
        return llamadasI;
    }

    public void setLlamadasI(int llamadasI) {
        this.llamadasI = llamadasI;
    }

    public double getCostoTotal() {
        return costoTotal;
    }

    public void setCostoTotal(float costoTotal) {
        this.costoTotal = costoTotal;
    }

    @Override
    public String toString() {
        return "SumarioAgente{" + "a\u00f1o=" + año + ", mes=" + mes + ", codigo=" + codigo + ", codigoPais=" + codigoPais + ", llamadasC=" + llamadasC + ", llamadasI=" + llamadasI + ", costoTotal=" + costoTotal + '}';
    }
    
}
