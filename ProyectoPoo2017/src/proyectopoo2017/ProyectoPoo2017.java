/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectopoo2017;

import Empresa.Cliente;
import Empresa.Proveedor;
import Empresa.SumarioAgente;
import Miscelaneos.Ip;
import Miscelaneos.Archivo;
import Miscelaneos.Pais;
import Miscelaneos.Tarifa;
import Miscelaneos.Llamada;
import Miscelaneos.LlamadaFacturada;
import Usuario.Admin;
import Usuario.Tecnico;
import Usuario.Usuario;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Javier, David y Galo
 */
public class ProyectoPoo2017 {    

    /**
     *
     */
    public static ArrayList<Usuario> listaUsuario= new ArrayList<Usuario>();

    /**
     *
     */
    public static ArrayList<Pais> listaPaises= new ArrayList<Pais>();

    /**
     *
     */
    public static ArrayList<Proveedor> listaProveedores= new ArrayList<Proveedor>();

    /**
     *
     */
    public static ArrayList<Ip> listaIps= new ArrayList<Ip>();

    /**
     *
     */
    public static ArrayList<Cliente> listaClientes= new ArrayList<Cliente>();

    /**
     *
     */
    public static ArrayList<Tarifa> listaTarifas= new ArrayList<Tarifa>();

    /**
     *
     */
    public static ArrayList<Llamada> listaLlamadas= new ArrayList<Llamada>();

    /**
     *
     */
    public static ArrayList<LlamadaFacturada> listaLlamadasFacturadas= new ArrayList<LlamadaFacturada>();

    /**
     * @param args the command line arguments
     * @throws java.lang.InterruptedException
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws InterruptedException, IOException {
        Archivo archivo=new Archivo();
        inicializar();
    Scanner input= new Scanner(System.in);
    Boolean finalizo=false;
    while(finalizo==false){
        System.out.println("Ingrese su usuario:");
    String usuario= input.next();
    System.out.println("Ingrese la contraseña:");
    String contraseña= input.next();
    boolean alerta= false;
   
    
    for (Usuario user:listaUsuario){
        if (user.getUsuario().equals(usuario) & user.getContraseña().equals(contraseña) & user.getNivel().equals("tecnico")){  
            Tecnico tecnico=new Tecnico(user.getUsuario(),user.getContraseña(),user.getNombre(),user.getNivel());
            int opcion=0;
            while (opcion!=7){

            System.out.println(
            "Menú Técnico\n"+
            "1. Información de Clientes\n"+
            "2. Información de Proveedores\n"+
            "3. Información de Países\n"+
            "4. Administrar IPs\n"+
            "5. Información de Tarifa de Región\n"+
            "6. Facturar llamadas\n"+
            "7.Salir\n");
            opcion=input.nextInt();
            switch(opcion){
                case 1:
                    tecnico.mostrarClientes(listaClientes);
                    System.out.println("Desea editar los clientes.(si o no)");
                    String respuesta=input.next();
                    while (!(respuesta.equals("si")||respuesta.equals("no"))) {
                        if(respuesta.equals("si")){
                             tecnico.editarClientes(listaClientes);          
                             tecnico.mostrarClientes(listaClientes);
                        } else if (respuesta.equals("no")) break;
                        else {System.out.println("Ingrese si o no: "); respuesta = input.next();}
                    } break;
                case 2:
                    tecnico.mostrarProveedores(listaProveedores);
                    System.out.println("Desea editar los clientes.(si o no)");
                    String respuesta1=input.next();
                    while (!(respuesta1.equals("si")||respuesta1.equals("no"))) {
                        if(respuesta1.equals("si")){
                             tecnico.editarProveedores(listaProveedores);
                             tecnico.mostrarProveedores(listaProveedores);
                        } else if (respuesta1.equals("no")) break;
                        else {System.out.println("Ingrese si o no: "); respuesta1 = input.next();}
                    }break;  
                case 3:
                    tecnico.mostrarPaises(listaPaises);
                    System.out.println("Desea editar la información de los paises.(si o no)");
                    String respuesta2=input.next();
                    while (!(respuesta2.equals("si")||respuesta2.equals("no"))) {
                        if(respuesta2.equals("si")){
                             tecnico.editarPaises(listaPaises);
                             tecnico.mostrarPaises(listaPaises);
                        } else if (respuesta2.equals("no")) break;
                        else {System.out.println("Ingrese si o no: "); respuesta2 = input.next();}
                    }break;                                       
                case 4:
                    tecnico.mostrarIps(listaIps);
                    int opcionip=0;
                    while(opcionip!=3){
                    System.out.println("\n1. Agregar Ip\n"+
                                       "2. Editar Ip\n"+
                                       "3. Volver al Menú");
                    opcionip=input.nextInt();
                        switch (opcionip){
                            case 1:
                                tecnico.agregarIps(listaIps);
                                tecnico.mostrarIps(listaIps);
                                break;
                            case 2:   
                                tecnico.editarIps(listaIps);
                                tecnico.mostrarIps(listaIps);
                                break;
                        }
                    }
                    break;
                case 5:
                    tecnico.mostrarTarifas(listaTarifas);
                    System.out.println("Desea editar las tarifas?. (si o no)");
                    String respuestaEdt=input.next();
                    while (!(respuestaEdt.equals("si")||respuestaEdt.equals("no"))){
                        System.out.println("Ingrese si o no: ");
                        respuestaEdt = input.next();
                    }
                    if (respuestaEdt.equals("si")){
                        tecnico.editarTarifas(listaTarifas);
                        tecnico.mostrarTarifas(listaTarifas);                        
                    }

                    break;
                case 6:
                    listaLlamadasFacturadas = tecnico.facturarLlamadas(listaLlamadas, listaIps, listaTarifas, listaClientes);                      
                    System.out.println("Proceso terminado");
                    break;
                default:
                    finalizo=true;
                    break;
            }                

            }
            alerta=true;        
            break;
            }
        
    
    if (user.getUsuario().equals(usuario) & user.getContraseña().equals(contraseña) & user.getNivel().equals("admin")){
        File file=new File("src/Archivos/llamadas_facturadas.csv");
        int opcion=0;
         Admin admin=new Admin(user.getUsuario(),user.getContraseña(),user.getNombre(),user.getNivel());
        if(!file.exists()){
            System.out.println("No hay informacion de facturacion");
            opcion=4;
        }   
        inicializarAdmin();
        ArrayList<ArrayList<SumarioAgente>> sumarioClientes=new ArrayList<>();
;
                   for(Cliente c:listaClientes){
                      ArrayList<SumarioAgente> array = admin.sumarioCliente(listaLlamadasFacturadas,c.getCodigo() );
                         sumarioClientes.add(array);
                    }
                   ArrayList<ArrayList<SumarioAgente>> sumarioProveedores=new ArrayList<>();
                   for(Proveedor c:listaProveedores){
                      ArrayList<SumarioAgente> array = admin.sumarioProveedor(listaLlamadasFacturadas,c.getCodigo() );
                         sumarioProveedores.add(array);
                    }
                   
         while (opcion!=4){        
            System.out.println(
            "Menu Admistrador\n"+
            "1. Detalle Factura Cliente\n"+
            "2. Reporte de llamadas por clientes por mes\n"+
            "3. Reporte de llamadas por proveedor por mes\n"+
            "4.Salir\n");          
            opcion=input.nextInt();
            
            
            Scanner sc=new Scanner(System.in);
            switch(opcion){
                case 1:
                     
                     System.out.println("Ingrese codigo: ");
                     String cod= sc.next();
                     System.out.println("Ingrese mes en dos digitos .ejem(09): ");
                     String mes =sc.next();
                     ArrayList<String> detalleFactura=admin.detalleFactura(cod, mes, sumarioClientes, listaClientes);
                     for(String s: detalleFactura){
                         System.out.println(s);
                     }
                    break;
                case 2:
                    System.out.println("Ingrese mes: ");
                    String mes2=sc.next();
                    ArrayList<ArrayList<String>> reporteLlamadas=admin.reporteLlamadasClientes(mes2, sumarioClientes, listaClientes, listaPaises);
                    for(ArrayList<String> a: reporteLlamadas){
                        for(String b: a){
                            System.out.print(b+",");
                        }
                        System.out.print("\n");
                        System.out.println("--------------------------");
                    }
                    break;
                case 3:
                    System.out.println("Ingrese mes: ");
                    String mes3=sc.next();
                    ArrayList<ArrayList<String>> reporteLlamadasP=admin.reporteLlamadasProveedores(mes3, sumarioProveedores, listaProveedores, listaPaises);
                    for(ArrayList<String> a: reporteLlamadasP){
                        for(String b: a){
                            System.out.print(b+",");
                        }
                        System.out.print("\n");
                        System.out.println("--------------------------");
                    }
                    break;
                default:
                    finalizo=true;
                    break;
            }                
                          
        }
        alerta=true;
        break;
        }
        
    }
    if (!alerta){
        System.out.println("No existe el usuario");
    }
    }
     
        
    }

    /**
     *
     */
    public static void inicializar(){
        Archivo archivo= new Archivo();
        List<String> usuarios=archivo.leerArchivo("usuarios");
        for(String s: usuarios){
            String[] sp=s.split(",");
            listaUsuario.add(new Usuario(sp[0],sp[1],sp[2],sp[3]));
        }
        
        List<String> paises=archivo.leerArchivo("paises");
        for(String s: paises){
            String[] sp=s.split(",");
            listaPaises.add(new Pais(sp[0],sp[1],sp[2]));
        }
        List<String> proveedores=archivo.leerArchivo("proveedores");
        for(String s: proveedores){
            String[] sp=s.split(",");
            listaProveedores.add(new Proveedor(sp[0],sp[1],sp[2],sp[3]));
        }
        List<String> ips=archivo.leerArchivo("ips");
        for(String s: ips){
            String[] sp=s.split(",");
            listaIps.add(new Ip(sp[0],sp[1],sp[2]));
        }
        List<String> clientes=archivo.leerArchivo("clientes");
        for(String s: clientes){
            String[] sp=s.split(",");
            listaClientes.add(new Cliente(sp[0],sp[1],sp[2],sp[3],sp[4]));
        }
        List<String> tarifas=archivo.leerArchivo("tarifas");
        for(String s: tarifas){
            String[] sp=s.split(",");
            listaTarifas.add(new Tarifa(sp[0],sp[1],sp[2],sp[3],Float.valueOf(sp[4]),sp[5]));
        }
        List<String> llamadas=archivo.leerArchivo("llamadas");
        for(String s: llamadas){
            String[] sp=s.split(",");
            listaLlamadas.add(new Llamada(sp[0],sp[1],sp[2],sp[3],sp[4],sp[5],sp[6],Integer.valueOf(sp[7])));
        }
    }

    /**
     *
     */
    public static void inicializarAdmin(){
        Admin admin = new Admin();
          Archivo archivo= new Archivo();
        List<String> facturadas=archivo.leerArchivo("llamadas_facturadas");
        for(String s: facturadas){
            String[] sp=s.split(",");
            listaLlamadasFacturadas.add(new LlamadaFacturada(sp[0],sp[1],sp[2],sp[3],sp[4],sp[5],sp[6],Integer.valueOf(sp[7]),sp[8],sp[9],sp[10],Float.valueOf(sp[11]),Float.valueOf(sp[12])));
        }
        
               
    }
    
}
